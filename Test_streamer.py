from ibapi.client import EClient
from ibapi.wrapper import EWrapper
from threading import Thread
from ibapi.contract import Contract
import time
import math
class Streamer(EWrapper, EClient):
	def __init__(self, creds:dict):
		EClient.__init__(self, self)
		self.CREDS = creds
		self.subscribedTickers = []
		self.connect()
		self.streaming = True
		

	def tickPrice(self, reqId, tickType, price: float, attrib):
		print("For reqId ",reqId,"price is ",price)
		index=reqId/3
		index=math.ceil(index)-1
		if reqId%3==1:
			self.subscribedTickers[index].current_stk_price=price
		elif reqId%3==2:
			self.subscribedTickers[index].current_call_price=price
		else:
			self.subscribedTickers[index].current_put_price=price
	def subscribeTicker(self,ticker):
			self.subscribedTickers.append(ticker)
			# print("---------",ticker.stk_contract)
			# print("=====",ticker.call_contract)
			# print("-=-=-=",ticker.put_contract)
			

	def connect(self) -> None:
		"""
		Connect the system with Interactive brokers TWS desktop app\n
		"""
		super().connect(self.CREDS['host'], int(self.CREDS['port']), 1)
		Thread(target=self.run, daemon=True).start()

		time.sleep(1)
	def startStreaming(self):
		for ticker in self.subscribedTickers:
			print("---------",ticker.stk_contract)
			print("=====",ticker.call_contract)
			print("-=-=-=",ticker.put_contract)
			self.reqMktData(1, ticker.stk_contract, '', False, False, [])
			self.reqMktData(2,ticker.call_contract, '', False, False, [])
			self.reqMktData(3,ticker.put_contract, '', False, False, [])
				
	def stop_streaming(self) -> None:
	    self.streaming=False

	# def istickersubscribed(self,ticker_symbol) -> bool:
		
	# 	for ticker in self.subscribedTickers:
	# 		if ticker_symbol == ticker.symbol:
	# 			return True
	# 	return False