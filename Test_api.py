from ibapi.client import EClient
from ibapi.wrapper import EWrapper  
import time
from threading import Thread
from ibapi.contract import Contract
from ibapi.order import Order
import yfinance as yf

class IBapi(EWrapper, EClient):
     def __init__(self,creds:dict):
         EClient.__init__(self, self) 
         self.CREDS=creds
         self.orderId=None
         self.order_status = {}


     def connect(self):
        super().connect(self.CREDS['host'], self.CREDS['port'], self.CREDS.get('client_id') or 1)
        if(super().isConnected()):
            print("TWS Connection Status : Connected Successfully")
        else:
            print("Issue while connecting with TWS")
        
        Thread(target=self.run, daemon=True).start()
        time.sleep(1)

     def create_stk_contract(self, symbol, sec_type, exchange, currency):
        contract_obj=Contract()
        contract_obj.symbol = symbol
        contract_obj.secType = sec_type
        contract_obj.exchange = exchange
        contract_obj.currency = currency
        return contract_obj   

     def create_opt_contract(self, symbol, sec_type, exchange, currency, expiry=None, strike=None, right=None):
        contract_obj=Contract()
        contract_obj.symbol = symbol
        contract_obj.secType = sec_type
        contract_obj.exchange = exchange
        contract_obj.currency = currency
        contract_obj.multiplier = '100'
        if expiry is not None:
           contract_obj.lastTradeDateOrContractMonth = expiry
        if strike is not None:
            contract_obj.strike = strike
        if right is not None:
            contract_obj.right = right
        return contract_obj
      
     def get_exit_side(self,current_side:str):
         if current_side=="BUY":
             return "SELL"
         else:
             return "BUY"

     def orderStatus(self, orderId, status, filled, remaining, avgFullPrice, permId, parentId, lastFillPrice, clientId, whyHeld, mktCapPrice):
        print('orderStatus - orderid:', orderId, 'status:', status, 'filled', filled, 'remaining', remaining, 'lastFillPrice', lastFillPrice)
        
     def openOrder(self, orderId, contract, order, orderState):
        print('openOrder id :', orderId, contract.symbol, contract.secType, '@', contract.exchange, ':', order.action, order.orderType, order.totalQuantity, orderState.status)
     
     def execDetails(self, reqId, contract, execution):
         print('Order Executed : ', reqId, contract.symbol, contract.secType, contract.currency, execution.execId, execution.orderId, execution.shares, execution.lastLiquidity)

     
     def place_order(self,contact_object, action, quantity,target_percentage,stop_loss_percentage,trailing_percentage,ticker):
        current_ticker=yf.Ticker(ticker.symbol)
        if contact_object.right == "C":
            current_ticker.option_chain
        
        current_price = 5 # Replace with current price of the option
        target_price = round(current_price * (1 + target_percentage/100), 2)
        stop_loss_price = round(current_price * (1 - stop_loss_percentage/100), 2)
        order=Order()
        order.orderId=self._get_next_order_id()
        order.action = action
        order.orderType = "LMT"
        order.tif = "GTC"
        order.lmtPrice = current_price
        order.totalQuantity = quantity
        order.eTradeOnly = False
        order.firmQuoteOnly = False
        order.transmit = False
        
        print("Placing normal order : ",order.orderId)
       
        

        tp_order=Order()
        tp_order.orderId=self._get_next_order_id()
        tp_order.action=self.get_exit_side(order.action)
        tp_order.totalQuantity = quantity
        tp_order.orderType = "LMT"
        tp_order.tif = "GTC"
        tp_order.lmtPrice = target_price
        tp_order.parentId = order.orderId
        tp_order.eTradeOnly = False
        tp_order.transmit = False
        tp_order.firmQuoteOnly = False
        ticker.prev_tp_order = tp_order
        print("Placing target order with orderID : ",tp_order.orderId)
        


        sl_order = Order()
        sl_order.orderId = self._get_next_order_id()
        sl_order.action = self.get_exit_side(order.action)
        sl_order.totalQuantity = quantity
        sl_order.orderType = "TRAIL"
        sl_order.tif = "GTC"
        # sl_order.lmtPrice = stop_loss_price
        # sl_order.auxPrice = stop_loss_price
        sl_order.trailingPercent = trailing_percentage
        # sl_order.parentId = order.orderId
        sl_order.transmit = True
        sl_order.eTradeOnly = False
        sl_order.firmQuoteOnly = False
        ticker.prev_sl_order = sl_order
        print("Placing stoploss order with orderID : ",sl_order.orderId)

        self.placeOrder(order.orderId,contact_object,order)
        self.placeOrder(tp_order.orderId,contact_object,tp_order)
        self.placeOrder(sl_order.orderId,contact_object,sl_order)
        return 
     

     def nextValidId(self, orderId:int):
        self.orderId = orderId

     
     def _get_next_order_id(self):
        """get the current class variable order_id and increment
        it by one.
        """
        current_order_id = self.orderId
        self.orderId += 1
        return current_order_id