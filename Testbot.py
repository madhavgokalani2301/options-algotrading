from ibapi.client import EClient
from ibapi.wrapper import EWrapper
from ibapi.contract import Contract
import numpy as np
import yfinance as yf
import pandas as pd
import pandas_ta as ta
# from api_ib import IBTWSAPI
from Test_api import IBapi
from ibapi.order import Order
from Test_streamer import Streamer


# Trimmer % is used to sell a porion of quantity when trader is in give % profit.

class Ticker:
	def __init__(self) -> None:
		self.symbol=""
		self.quantity= ""
		self.target_profit_prcentage = ""
		self.stoploss_percentage = ""
		self.trimmer_percentage = ""
		self.trailing_percentage= ""
		self.Expiry=""
		self.strike_price = ""
		self.current_stk_price=None
		self.current_call_price=None
		self.current_put_price=None
		self.stk_contract=Contract()
		self.call_contract=Contract()
		self.put_contract=Contract()
		self.prev_sl_order=Order()
		self.prev_tp_order=Order()



class Bot:
	def __init__(self,settings):
		creds = {
        "account": "IB",
        "host": "127.0.0.1",
        "port": 7496,
        "client_id": 1
        }
		self.ibapi = IBapi(creds=creds)
		self.Ticker_List = []
		self.streamer=Streamer(creds=creds)
		for setting in settings:
			ticker = Ticker()
			ticker.symbol=setting["Symbol"]
			ticker.quantity=setting["Quantity"]
			ticker.target_profit_prcentage = setting["target_profit_prcentage"]
			ticker.stoploss_percentage = setting["stoploss_percentage"]
			ticker.trimmer_percentage = setting["trimmer_percentage"]
			ticker.trailing_percentage= setting["trailing_percentage"]
			ticker.Expiry=setting["Expiry"]
			ticker.strike_price = setting["Strike_price"]
			ticker.stk_contract=self.ibapi.create_stk_contract(symbol=ticker.symbol, sec_type="STK", exchange="SMART", currency="USD")
			ticker.call_contract=self.ibapi.create_opt_contract(symbol=ticker.symbol, sec_type="OPT", exchange="SMART", currency="USD", expiry="20230526", strike=ticker.strike_price, right="C")
			ticker.put_contract = self.ibapi.create_opt_contract(symbol=ticker.symbol, sec_type="OPT", exchange="SMART", currency="USD", expiry="20230526", strike=ticker.strike_price, right="P")
			self.Ticker_List.append(ticker)
			self.streamer.subscribeTicker(ticker)

	def param_status(self,ticker_data):
		if(ticker_data['CCI'].iloc[-1] > 0):
			print("CCI value is above 0. CCI :",ticker_data['CCI'].iloc[-1])
		else:
			print("CCI value is below 0. CCI :",ticker_data['CCI'].iloc[-1])
		
		if(ticker_data['EMA20'].iloc[-1] > ticker_data['MA50'].iloc[-1]):
			print("EMA20 value is above MA50. EMA20 :",ticker_data['EMA20'].iloc[-1],"MA50 :", ticker_data['MA50'].iloc[-1])
		else:
			print("EMA20 value is below MA50. EMA20 :",ticker_data['EMA20'].iloc[-1],"MA50 :", ticker_data['MA50'].iloc[-1])

		if( ticker_data['RSI'].iloc[-1] > ticker_data['RSI_SMA_14'].iloc[-1]):
			print("RSI value is above RSI_SMA_14. RSI :",ticker_data['RSI'].iloc[-1] , "RSI_SMA_14 : ",ticker_data['RSI_SMA_14'].iloc[-1] )
		else:
			print("RSI value is below RSI_SMA_14. RSI :",ticker_data['RSI'].iloc[-1] , "RSI_SMA_14 : ",ticker_data['RSI_SMA_14'].iloc[-1] )

	def should_buy_call(self,ticker_data):
		can_we_buy=False
		if ticker_data['CCI'].iloc[-1] > 0 and ticker_data['EMA20'].iloc[-1] > ticker_data['MA50'].iloc[-1] and ticker_data['RSI'].iloc[-1] > ticker_data['RSI_SMA_14'].iloc[-1] :
			can_we_buy=True
		return can_we_buy

	def should_buy_put(self,ticker_data):
		can_we_buy=False
		if ticker_data['CCI'].iloc[-1] < 0 and ticker_data['EMA20'].iloc[-1] < ticker_data['MA50'].iloc[-1] and ticker_data['RSI'].iloc[-1] < ticker_data['RSI_SMA_14'].iloc[-1] :
			can_we_buy=True
		return can_we_buy

	def calculate_macd(self,ticker_data):
		ticker_data.ta.macd(close='close', fast=12, slow=26, signal=9, append=True)
		return

	def are_prev_open_orders(self,ticker):
		return False
		open_orders = self.ibapi.reqOpenOrders()
		print(open_orders)
		for order in open_orders:
			if order.orderId == ticker.prev_tp_order.orderId or order.orderId == ticker.prev_sl_order.orderId:
				return True
		return False
	


	def main(self):
		self.ibapi.connect()
		self.streamer.startStreaming()
		pd.set_option('display.max_columns', None)
		condition = True
		while(condition):
			for ticker in self.Ticker_List:
				print("stock price",ticker.current_stk_price, "For",ticker.symbol)
				print("stock current_call_price",ticker.current_call_price, "For",ticker.symbol)
				print("stock current_put_price",ticker.current_put_price, "For",ticker.symbol)
				ticker_data= yf.download(ticker.symbol,period="2d",interval='5m')
				ticker_data['EMA20']=ta.ema(ticker_data['Close'],length=20)
				ticker_data['MA50']=ta.sma(ticker_data['Close'],50)
				ticker_data['RSI']=ta.rsi(ticker_data['Close'])
				ticker_data['RSI_SMA_14']=ta.sma(close=ticker_data['RSI'],length=14)
				ticker_data['CCI']=ta.cci(ticker_data['High'],ticker_data['Low'],ticker_data['Close'],20)
				self.calculate_macd(ticker_data)
				if( ticker_data['Open'].iloc[-1] == ticker_data['High'].iloc[-1] and ticker_data['Open'].iloc[-1] == ticker_data['Low'].iloc[-1] and ticker_data['Open'].iloc[-1]==ticker_data['Close'].iloc[-1]):
					ticker_data=ticker_data[:-1]
				print("Current condition for",ticker.symbol)
				# print(ticker_data)
				self.param_status(ticker_data)
				
				
				if(self.should_buy_call(ticker_data)):
					print("Condition fit for buying CALL of ",ticker.symbol)
					if (self.are_prev_open_orders(ticker)):
						print ("Can't Buy because open  orders are there")
					else:
						print("Buying the call",ticker.symbol)
						# self.ibapi.place_order(ticker.call_contract,"BUY",ticker.quantity,ticker.target_profit_prcentage,ticker.stoploss_percentage,ticker.trailing_percentage,ticker)

				elif(self.should_buy_put(ticker_data)):
					if (self.are_prev_open_orders(ticker)):
						print ("Can't Buy because open  orders are there")
					else:
						print("Condition fit for buying PUT of ",ticker.symbol)
						# self.ibapi.place_order(ticker.put_contract,"BUY",ticker.quantity,ticker.target_profit_prcentage,ticker.stoploss_percentage,ticker.trailing_percentage,ticker)
				
				else:
					print("NO ORRDER")
				condition=False
			
